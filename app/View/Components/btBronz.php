<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class btBronz extends Component
{
    /**
     * Create a new component instance.
     */
    public $tCofBut;

    public function __construct($tCofBut)
    {
        $this->tCofBut=$tCofBut;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.Buttons.bt-bronz');
    }
}
