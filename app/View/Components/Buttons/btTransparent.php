<?php

namespace App\View\Components\Buttons;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class btTransparent extends Component
{
    /**
     * Create a new component instance.
     */
    public $tCofButT;
    public function __construct($tCofButT)
    {
        //
        $this->tCofButT=$tCofButT;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.buttons.bt-transparent');
    }
}
